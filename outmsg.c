#include <stdio.h>
#include <stdarg.h>

#include "util.h"
#include "game_consts.h"
#include "outmsg.h"

void cltprintf(client *clt, char *fmt, ...);

void outmsg_sys_login_accept(client *clt, char *motd) {
    cltprintf(clt, "login_accept %s\n", motd);
}

void outmsg_sys_disconnect(client *clt, char *reason) {
    cltprintf(clt, "disconnect %s\n", reason);
}

void outmsg_sys_error(client *clt, char *message) {
    char transform_buffer[2048];
    unsigned long i;

    for (i = 0; *message && i < sizeof (transform_buffer) - 1; message++, i++) {
        if (*message == '\n') {
            transform_buffer[i++] = '\\';
            transform_buffer[i] = 'n';
            continue;
        }
        if (*message == '\\') {
            transform_buffer[i++] = '\\';
            transform_buffer[i] = '\\';
            continue;
        }

        transform_buffer[i] = *message;
    }

    transform_buffer[i] = '\0';
    cltprintf(clt, "error %s\n", transform_buffer);
}

void outmsg_sys_ping(client *clt, uuid_t uuid, int latency) {
    char uuidbuffer[UUID_STR_LEN];

    uuid_unparse(uuid, uuidbuffer);

    if (latency >= 0) {
        cltprintf(clt, "ping %s %d\n", uuidbuffer, latency);
    } else {
        cltprintf(clt, "ping %s\n", uuidbuffer);
    }
}

void outmsg_lobby_game_name(client *clt, uuid_t uuid, char *name) {
    char uuidbuffer[UUID_STR_LEN];

    uuid_unparse(uuid, uuidbuffer);
    cltprintf(clt, "lobby_game_name %s %s\n", uuidbuffer, name);
}

void outmsg_lobby_game_state(client *clt, uuid_t uuid, int state) {
    char uuidbuffer[UUID_STR_LEN];
    char *state_cenum;

    switch(state) {
        case GAME_STATE_AWAITING_PLAYER:
            state_cenum = GAME_STATE_AWAITING_PLAYER_CENUM;
            break;
        case GAME_STATE_PLAYING:
            state_cenum = GAME_STATE_PLAYING_CENUM;
            break;
        case GAME_STATE_CONNECTION_PROBLEM:
            state_cenum = GAME_STATE_CONNECTION_PROBLEM_CENUM;
            break;
        default:
            return;
    }

    uuid_unparse(uuid, uuidbuffer);
    cltprintf(clt, "lobby_game_state %s %s\n", uuidbuffer, state_cenum);
}

void outmsg_lobby_game_remove(client *clt, uuid_t uuid) {
    char uuidbuffer[UUID_STR_LEN];

    uuid_unparse(uuid, uuidbuffer);
    cltprintf(clt, "lobby_game_remove %s\n", uuidbuffer);
}

void outmsg_lobby_reset(client *clt) {
    cltprintf(clt, "lobby_reset\n");
}

void outmsg_join_accept(client *clt, int playerno, int rejoin_key) {
    cltprintf(clt, "join_accept %d %d\n", playerno, rejoin_key);
}

void outmsg_game_meta(
        client *clt,
        double surface_width,
        double surface_height,
        double ball_size,
        double players_width,
        double players_height,
        double players_offset,
        int win_score) {

    cltprintf(clt, "game_meta %lf %lf %lf %lf %lf %lf %d\n",
            surface_width,
            surface_height,
            ball_size,
            players_width,
            players_height,
            players_offset,
            win_score);
}

void outmsg_player_name(client *clt, int playerno, char *name) {
    cltprintf(clt, "player_name %d %s\n", playerno, name);
}

void outmsg_game_scores(client *clt, int scorea, int scoreb) {
    cltprintf(clt, "game_scores %d %d\n", scorea, scoreb);
}

void outmsg_start_timeout(client *clt, int timeout) {
    cltprintf(clt, "start_timeout %d\n", timeout);
}

void outmsg_ball(client *clt, double xpos, double ypos, double xvel, double yvel) {
    cltprintf(clt, "ball %lf %lf %lf %lf\n", xpos, ypos, xvel, yvel);
}

void outmsg_paddle(client *clt, int player, double ypos, double yvel) {
    cltprintf(clt, "paddle %d %lf %lf\n", player, ypos, yvel);
}

void outmsg_game_finished(client *clt, char *winner) {
    cltprintf(clt, "game_finished %s\n", winner);
}

void outmsg_connection_error(client *clt, char *desc) {
    cltprintf(clt, "connection_error %s\n", desc);
}

void outmsg_connection_restored(client *clt) {
    cltprintf(clt, "connection_restored\n");
}

void cltprintf(client *clt, char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    if (!fd_valid(clt->socket)) {
        return;
    }

    vdprintf(clt->socket, fmt, args);
    va_end(args);
}
