
#ifndef UPS_SP_CMQ_H
#define UPS_SP_CMQ_H

#include <pthread.h>

#include "inmsg.h"

/**
 * Client Message Queue entry. Not to be manipulated by the user.
 */
typedef struct _cmq_entry {
    inmsg *msg;
    struct _cmq_entry *next;
} cmq_entry;

/**
 * Client Message Queue. To be manipulated only through the assigned cmq_ functions.
 */
typedef struct {
    pthread_mutex_t mutex;
    cmq_entry *head;
    cmq_entry *tail;
} cmq;

cmq *cmq_create(void);
void cmq_free(cmq **queue);
int cmq_push(cmq *queue, inmsg *msg);
inmsg *cmq_peek(cmq *queue);
inmsg *cmq_pop(cmq *queue);

#endif /* UPS_SP_CMQ_H */
