#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <uuid/uuid.h>

#include "logc/log.h"

#include "client.h"
#include "game.h"
#include "cmq.h"
#include "util.h"
#include "exit_codes.h"
#include "outmsg.h"
#include "server.h"

#define MESSAGE_DELIMITER '\n'
#define START_DESCRIPTOR 3

#define SELECT_TIMEOUT_TIME 100
#define PING_SLEEP_TIME 500

#define PING_SOFT_TIMEOUT_TIME 5000
#define PING_HARD_TIMEOUT_TIME 30000

char message_buffer[1024];
int running = 1;


void * ping_routine(void *);

int process_messages(client *clt, sdict *games, conf *config);


int server_main(conf *config, int srv_sock) {
    sdict *clients;
    sdict *games;
    sdict_iterator it;
    sdict_iterator jt;
    client *clt;
    game *gme;
    int retval;
    int result = EXIT_SUCCESS;
    int fd;
    int clt_sock;
    int fionread_size;
    int clients_changed;
    struct sockaddr_in clt_addr;
    socklen_t addr_len = sizeof(clt_addr);
    ssize_t recv_size;
    size_t remaining_size;
    fd_set socks, select_socks;
    pthread_t ping_thread;
    struct timeval select_timeout;

    clients = sdict_create(intkey_hash, intkey_equals, client_dict_entry, client_dict_unentry, SDICT_DEFAULT_SIZE);
    if (!clients) {
        log_fatal("Could not create clients dictionary (code: 0x%X)", sdict_error());
        exit(ERR_SYS_ALLOC);
    }

    games = sdict_create(uuid_hash, uuid_equals, game_entry_fn, game_unentry_fn, SDICT_DEFAULT_SIZE);
    if (!games) {
        log_fatal("Could not create games dictionary (code: 0x%X)", sdict_error());
        exit(ERR_SYS_ALLOC);
    }

    FD_ZERO(&socks);
    FD_SET(srv_sock, &socks);

    pthread_create(&ping_thread, NULL, ping_routine, clients);

    while (running) {
        select_socks = socks;

        select_timeout.tv_sec = 0;
        select_timeout.tv_usec = SELECT_TIMEOUT_TIME * MILLIS_TO_MICROS;
        retval = select(FD_SETSIZE, &select_socks, NULL, NULL, &select_timeout);

        if (retval < 0) {
            log_fatal("Error in select. (errno: %d)", errno);
            exit(ERR_SELECT);
        }

        clients_changed = 0;

        if (retval > 0) {
            for (fd = START_DESCRIPTOR; fd < FD_SETSIZE; fd++) {
                // Socket not active
                if (!FD_ISSET(fd, &select_socks)) {
                    continue;
                }

                // Accept new client
                if (fd == srv_sock) {
                    clt_sock = accept(srv_sock, (struct sockaddr *) &clt_addr, &addr_len);
                    FD_SET(clt_sock, &socks);
                    clt = client_create(clt_sock, &clt_addr);
                    if (!clt) {
                        log_fatal("Could not allocate client!");
                        exit(ERR_SYS_ALLOC);
                    }
                    if (clients->size >= (unsigned int) config->server_max_clients) {
                        clt->connection_state = CONNECTION_STATE_DISCONNECT_SERVER_FULL;
                    }
                    sdict_put(clients, &clt_sock, clt);

                    clients_changed = 1;
                    log_info("Client %s connected", client_logid(clt));

                    continue;
                }

                // Process client with current socket
                clt = sdict_get(clients, &fd);
                pthread_mutex_lock(&clt->mutex);
                ioctl(fd, FIONREAD, &fionread_size);
                if (fionread_size > 0) {
                    remaining_size = (size_t) (sizeof(message_buffer) - (clt->buffer_wpos - clt->buffer) - 1);
                    if (remaining_size <= 0) {
                        outmsg_sys_error(clt, "Your client is sending messages that are too long!");
                        log_error("Buffer of client %s has overflowed.", client_logid(clt));
                        clt->sending_garbage_incidents += CLIENT_MAX_GARBAGE_INCIDENTS / 2;
                        clt->buffer_wpos = clt->buffer;
						pthread_mutex_unlock(&clt->mutex);
                        continue;
                    }

                    recv_size = recv(fd, clt->buffer_wpos, remaining_size, 0);
                    clt->buffer_wpos[recv_size] = '\0';

                    memcpy(message_buffer, clt->buffer_wpos, (size_t) recv_size + 1);
                    clt->buffer_wpos += recv_size;

                    process_messages(clt, games, config);
                } else {
                    clt->connection_state = CONNECTION_STATE_DISCONNECT_COMMUNICATION_ERROR;
                }
                pthread_mutex_unlock(&clt->mutex);
            }
        }

        for (sdict_it(games, &it); it.current; sdict_it_next(&it)) {
            gme = it.current->value;

            pthread_mutex_lock(&gme->mutex);

            // final-state game cleanup
            if (gme->state == GAME_STATE_FINAL) {
                pthread_join(gme->pthread, NULL);

                for (sdict_it(clients, &jt); jt.current; sdict_it_next(&jt)) {
                    clt = jt.current->value;
                    pthread_mutex_lock(&clt->mutex);
                    if (clt->client_state != CLIENT_STATE_LOBBY) {
                        pthread_mutex_unlock(&clt->mutex);
                        continue;
                    }
                    outmsg_lobby_game_remove(clt, gme->uuid);
                    pthread_mutex_unlock(&clt->mutex);
                }
                sdict_it_unlock(&jt);

                if (gme->playera) {
                    pthread_mutex_lock(&gme->playera->mutex);
                    gme->playera->client_state = CLIENT_STATE_INIT_LOBBY;
                    pthread_mutex_unlock(&gme->playera->mutex);
                }
                if (gme->playerb) {
                    pthread_mutex_lock(&gme->playerb->mutex);
                    gme->playerb->client_state = CLIENT_STATE_INIT_LOBBY;
                    pthread_mutex_unlock(&gme->playerb->mutex);
                }
                pthread_mutex_unlock(&gme->mutex);
                sdict_it_remove(&it);
                continue;
            }

            // notify clients about changes in games
            if (!gme->name_notified || !gme->state_notified) {
                for (sdict_it(clients, &jt); jt.current; sdict_it_next(&jt)) {
                    clt = jt.current->value;
                    pthread_mutex_lock(&clt->mutex);

                    if (clt->client_state != CLIENT_STATE_LOBBY) {
                        pthread_mutex_unlock(&clt->mutex);
                        continue;
                    }

                    if (!gme->name_notified) {
                        outmsg_lobby_game_name(clt, gme->uuid, gme->name);
                        gme->name_notified = 1;
                    }

                    if (!gme->state_notified) {
                        outmsg_lobby_game_state(clt, gme->uuid, gme->state);
                        gme->state_notified = 1;
                    }
                    pthread_mutex_unlock(&clt->mutex);
                }
                sdict_it_unlock(&jt);
            }
            pthread_mutex_unlock(&gme->mutex);
        }
        sdict_it_unlock(&it);

        for (sdict_it(clients, &it); it.current; sdict_it_next(&it)) {
            clt = it.current->value;

            pthread_mutex_lock(&clt->mutex);
            // send game list if state is INIT_LOBBY
            if (clt->client_state == CLIENT_STATE_INIT_LOBBY) {
                outmsg_lobby_reset(clt);

                for (sdict_it(games, &jt); jt.current; sdict_it_next(&jt)) {
                    gme = jt.current->value;
                    pthread_mutex_lock(&gme->mutex);
                    outmsg_lobby_game_name(clt, gme->uuid, gme->name);
                    outmsg_lobby_game_state(clt, gme->uuid, gme->state);
                    pthread_mutex_unlock(&gme->mutex);
                }
                sdict_it_unlock(&jt);
                clt->client_state = CLIENT_STATE_LOBBY;
            }

            if (clt->sending_garbage_incidents > CLIENT_MAX_GARBAGE_INCIDENTS) {
                clt->connection_state = CONNECTION_STATE_DISCONNECT_SENDING_GARBAGE;
            }

            // disconnect client if state is NONE and is marked for disconnection
            if ((clt->connection_state & CONNECTION_STATE_TYPE_MASK) == CONNECTION_STATE_TYPE_DISCONNECT &&
                    (clt->client_state == CLIENT_STATE_NONE || clt->client_state == CLIENT_STATE_LOBBY)) {
                log_info("Disconnecting client %s. Reason: %s", client_logid(clt), client_disconnect_reason(clt));
                if (clt->connection_state != CONNECTION_STATE_DISCONNECT_TIMEOUT &&
                    clt->connection_state != CONNECTION_STATE_DISCONNECT_COMMUNICATION_ERROR) {

                    outmsg_sys_disconnect(clt, client_disconnect_reason(clt));
                }
                close(clt->socket);
                FD_CLR(clt->socket, &socks);
                sdict_it_remove(&it);
                clients_changed = 1;
            }
            pthread_mutex_unlock(&clt->mutex);
        }
        sdict_it_unlock(&it);

        if (clients_changed) {
            log_info("There are %d clients currently connected.", clients->size);
        }
    }

    running = 0;
    pthread_join(ping_thread, NULL);
    for (sdict_it(clients, &it); it.current; sdict_it_next(&it)) {
        close(*((int *) it.current->key));
    }
    sdict_it_unlock(&it);
    sdict_free(&clients);
    return result;
}


void * ping_routine(void *arg) {
    sdict_iterator it;
    client_ping *ping;
    sdict *clients;
    client *clt;
    unsigned long ts;

    clients = arg;

    while(running) {
        for (sdict_it(clients, &it); it.current; sdict_it_next(&it)) {
            clt = (client *) it.current->value;

            pthread_mutex_lock(&clt->mutex);
            if ((clt->connection_state & CONNECTION_STATE_TYPE_MASK) == CONNECTION_STATE_TYPE_DISCONNECT) {
                pthread_mutex_unlock(&clt->mutex);
                continue;
            }

            ts = mono_msec() - clt->last_pong;
            if (ts > PING_SOFT_TIMEOUT_TIME) {
                if (ts > PING_HARD_TIMEOUT_TIME) {
                    if (clt->connection_state != CONNECTION_STATE_DISCONNECT_TIMEOUT) {
                        clt->connection_state = CONNECTION_STATE_DISCONNECT_TIMEOUT;
                    }
                } else {
                    if (clt->connection_state != CONNECTION_STATE_SOFT_TIMING_OUT) {
                        log_info("Client %s has connection problems.", client_logid(clt));
                        clt->connection_state = CONNECTION_STATE_SOFT_TIMING_OUT;
                    }
                }
            } else if (clt->connection_state == CONNECTION_STATE_SOFT_TIMING_OUT) {
                log_info("Client %s has recovered.", client_logid(clt));
                clt->connection_state = CONNECTION_STATE_CONNECTED;
            }

            ping = malloc(sizeof(client_ping));
            if (!ping) {
                exit(ERR_SYS_ALLOC);
            }
            uuid_generate_random(ping->uuid);
            ping->timestamp = mono_msec();
            sdict_put(clt->pings, &(ping->uuid), ping);

            outmsg_sys_ping(clt, ping->uuid, clt->latency);
            pthread_mutex_unlock(&clt->mutex);
        }
        sdict_it_unlock(&it);
        usleep(PING_SLEEP_TIME * MILLIS_TO_MICROS);
    }

    return NULL;
}

int process_messages(client *clt, sdict *games, conf *config) {
    char buffer[CLIENT_MSG_BUFFER_SIZE];
    unsigned long len;
    inmsg *m;
    char *msgstart = clt->buffer;
    char *msgend = clt->buffer;
    char uuidbuffer[UUID_STR_LEN];
    client_ping *ping;
    game *gme;
    game_main_args *gme_args;
    int rejoin_success;

    while (*msgend) {
        if (*msgend == MESSAGE_DELIMITER) {
            len = msgend - msgstart;
            memcpy(buffer, msgstart, len);
            buffer[((buffer[len - 1] == '\r') ? (len - 1) : (len))] = '\0';

            m = parse_message(buffer);
            if (!m) {
                clt->sending_garbage_incidents++;
                log_warn("Client %s has sent garbage!", client_logid(clt));
                outmsg_sys_error(clt, "Your client has sent a malformed message!");
            } else if (m->type == INMSG_SYS_PONG) {
                ping = (client_ping *) sdict_get(clt->pings, ((inmsg_sys_pong *) m)->uuid);
                if (ping) {
                    sdict_remove(clt->pings, ((inmsg_sys_pong *) m)->uuid);
                    clt->latency = (int) (m->msec - ping->timestamp);
                    clt->last_pong = m->msec;
                } else {
                    uuid_unparse(((inmsg_sys_pong *) m)->uuid, uuidbuffer);
                    log_warn("Unknown ping with UUID: %s", uuidbuffer);
                }
            } else if (m->type == INMSG_SYS_LOGIN) {
                if (clt->client_state == CLIENT_STATE_NONE) {
                    if (strchk(((inmsg_sys_login *) m)->name, isalnumorspace, sizeof(((inmsg_sys_login *) m)->name))) {
                        strncpy(clt->name, ((inmsg_sys_login *) m)->name, sizeof(clt->name));
                        clt->client_state = CLIENT_STATE_INIT_LOBBY;
                        outmsg_sys_login_accept(clt, config->server_motd);
                        log_info("Client %s has logged in.", client_logid(clt), clt->name);
                    } else {
                        log_warn("Client %s has provided an invalid name! Marking for disconnection.", client_logid(clt));
                        clt->connection_state = CONNECTION_STATE_DISCONNECT_INVALID_NAME;
                    }
                }
            } else if (m->type == INMSG_SYS_LOGOUT) {
                log_debug("Client %s requested logout", client_logid(clt));
                if (clt->client_state == CLIENT_STATE_INGAME) {
                    clt->client_state = CLIENT_STATE_LEAVING_GAME;
                }
                clt->connection_state = CONNECTION_STATE_DISCONNECT_LOGOUT;
            } else if (m->type == INMSG_LOBBY_GAME_CREATE) {
                if (clt->client_state == CLIENT_STATE_LOBBY) {
                    if (strchk(((inmsg_lobby_game_create *) m)->name, isalnumorspace,
                               sizeof(((inmsg_lobby_game_create *) m)->name))) {
                        gme = game_create(((inmsg_lobby_game_create *) m)->name);
                        gme->playera = clt;
                        clt->client_state = CLIENT_STATE_INGAME;
                        outmsg_join_accept(clt, 0, gme->playera_key);
                        sdict_put(games, NULL, gme);

                        log_info("Client %s has created game '%s'.", client_logid(clt), gme->name);

                        gme_args = malloc(sizeof(gme_args));
                        gme_args->gme = gme;
                        gme_args->config = config;
                        pthread_create(&gme->pthread, NULL, game_main, gme_args);
                    } else {
                        log_warn("Client %s has provided an invalid game name!", client_logid(clt));
                        outmsg_sys_error(clt, "Invalid game name!");
                    }
                } else {
                    outmsg_sys_error(clt, "Games can only be joined from within the lobby.");
                }
            } else if (m->type == INMSG_LOBBY_JOIN) {
                if (clt->client_state == CLIENT_STATE_LOBBY) {
                    gme = sdict_get(games, ((inmsg_lobby_join *) m)->uuid);
                    if (gme) {
                        pthread_mutex_lock(&gme->mutex);
                        if (gme->state == GAME_STATE_AWAITING_PLAYER) {
                            log_info("Client %s has joined game '%s'", client_logid(clt), gme->name);
                            gme->state = GAME_STATE_PLAYING;
                            gme->state_notified = 0;
                            gme->playerb = clt;
                            clt->client_state = CLIENT_STATE_INGAME;
                            outmsg_join_accept(clt, 1, gme->playerb_key);
                        } else {
                            outmsg_sys_error(clt, "You can only join a game that is waiting for a second player!");
                        }
                        pthread_mutex_unlock(&gme->mutex);
                    } else {
                        outmsg_sys_error(clt, "No game with the specified UUID found.");
                    }
                } else {
                    outmsg_sys_error(clt, "Games can only be joined from within the lobby!");
                }
            } else if (m->type == INMSG_LOBBY_REJOIN) {
                if (clt->client_state == CLIENT_STATE_LOBBY) {
                    gme = sdict_get(games, ((inmsg_lobby_rejoin *) m)->uuid);
                    if (gme) {
                        pthread_mutex_lock(&gme->mutex);
						if (gme->playera_key == ((inmsg_lobby_rejoin *) m)->key) {
							gme->playera = clt;
							outmsg_join_accept(clt, 0, gme->playera_key);
							rejoin_success = 1;
						} else if (gme->playerb_key == ((inmsg_lobby_rejoin *) m)->key) {
							gme->playerb = clt;
							outmsg_join_accept(clt, 1, gme->playerb_key);
							rejoin_success = 2;
						} else {
							outmsg_sys_error(clt, "Invalid rejoin key!");
							rejoin_success = 0;
						}

						if (rejoin_success) {
							gme->state = GAME_STATE_PLAYING;
							clt->client_state = CLIENT_STATE_INGAME;
							outmsg_join_accept(clt, rejoin_success - 1, gme->playerb_key);
						}
                        pthread_mutex_unlock(&gme->mutex);
                    } else {
                        outmsg_sys_error(clt, "No game with the specified UUID found.");
                    }
                } else {
                    outmsg_sys_error(clt, "Games can only be joined from within the lobby!");
                }
            } else if (m->type == INMSG_GAME_LEAVE) {
                if (clt->client_state == CLIENT_STATE_INGAME) {
                    log_info("Client %s has requested to leave their game.", client_logid(clt));
                    clt->client_state = CLIENT_STATE_LEAVING_GAME;
                }
            } else if (m->type == INMSG_GAME_PADDLE) {
                if (clt->client_state == CLIENT_STATE_INGAME) {
                    cmq_push(clt->in_queue, m);
                }
            }

            msgstart = msgend + 1;
        }

        msgend++;
    }

    if (msgstart != clt->buffer) {
        len = msgend - msgstart;
        memcpy(buffer, msgstart, len);
        memcpy(clt->buffer, buffer, len);
        clt->buffer[len] = '\0';
        clt->buffer_wpos = clt->buffer;
    }

    return 0;
}
