#ifndef UPS_SP_UTIL_H
#define UPS_SP_UTIL_H

#define MILLIS_TO_MICROS 1000

/**
 * @return the current time of <code>CLOCK_MONOTONIC</code> in milliseconds
 */
unsigned long mono_msec(void);

/**
 * @param i a pointer to an integer value
 * @return a hashcode of the integer referred to by the pointer
 */
unsigned long intkey_hash(void *i);

/**
 * Checks whether two integers referred to by void pointers are equal.
 *
 * @param a pointer to integer <em>a</em>
 * @param b pointer to integer <em>b</em>
 * @return <code>1</code> if values are equal, otherwise <code>0</code>
 */
int intkey_equals(void *a, void *b);

/**
 * @param uuid a pointer to an UUID value
 * @return a hashcode of the UUID referred to by the pointer
 */
unsigned long uuid_hash(void *uuid);

/**
 * Checks whether two UUIDs referred to by void pointers are equal.
 *
 * @param uua pointer to UUID <em>a</em>
 * @param uub pointer to UUID <em>b</em>
 * @return <code>1</code> if values are equal, otherwise <code>0</code>
 */
int uuid_equals(void *uua, void *uub);

/**
 * Checks whether the provided character is an alnum or a space.
 *
 * @param c the character to check
 */
int isalnumorspace(int c);

/**
 * Used for checking all the characters in the specified string with the <code>ctype.h</code> (or similarly behaving)
 * functions.
 *
 * @param str the string to check
 * @param chkfn the function to use for checking
 * @param limit maximum size of the string (0 means no limit)
 * @return <code>1</code> if <code>chkfn</code> returns non-zero values for each of the characters in the string,
 * otherwise <code>0</code>
 */
int strchk(char *str, int (*chkfn)(int), size_t limit);

/**
 * Checks whether the specified file descriptor is valid.
 *
 * @param fd the descriptor to check
 */
int fd_valid(int fd);

#endif /* UPS_SP_UTIL_H */
