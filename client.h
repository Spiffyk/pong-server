#ifndef UPS_SP_CLIENT_H
#define UPS_SP_CLIENT_H

#include <netinet/in.h>

#include "cmq.h"
#include "sdict/sdict.h"
#include "client_consts.h"


typedef struct {
    pthread_mutex_t mutex;
    unsigned int client_state;
    unsigned int connection_state;
    unsigned int sending_garbage_incidents;
    int socket;
    char name[CLIENT_NAME_LENGTH];
    struct sockaddr_in addr;
    char buffer[CLIENT_MSG_BUFFER_SIZE];
    char *buffer_wpos;
    cmq *in_queue;
    int latency;
    unsigned long last_pong;
    sdict *pings;
} client;

typedef struct {
    uuid_t uuid;
    unsigned long timestamp;
} client_ping;

int client_dict_entry(sdict_entry *entry);

int client_dict_unentry(sdict_entry *entry);

int client_ping_entry(sdict_entry *entry);

int client_ping_unentry(sdict_entry *entry);

client *client_create(int socket, struct sockaddr_in *addr);

void client_free(client **clt);

char *client_logid(client *clt);

char *client_disconnect_reason(client *clt);

#endif /* UPS_SP_CLIENT_H */
