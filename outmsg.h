#ifndef UPS_SP_OUTMSG_H
#define UPS_SP_OUTMSG_H

#include "client.h"

void outmsg_sys_login_accept(client *clt, char *motd);

void outmsg_sys_disconnect(client *clt, char *reason);

void outmsg_sys_error(client *clt, char *message);

void outmsg_sys_ping(client *clt, uuid_t uuid, int latency);

void outmsg_lobby_game_name(client *clt, uuid_t uuid, char *name);

void outmsg_lobby_game_state(client *clt, uuid_t uuid, int state);

void outmsg_lobby_game_remove(client *clt, uuid_t uuid);

void outmsg_lobby_reset(client *clt);

void outmsg_join_accept(client *clt, int playerno, int rejoin_key);

void outmsg_game_meta(
        client *clt,
        double surface_width,
        double surface_height,
        double ball_size,
        double players_width,
        double players_height,
        double players_offset,
        int win_score);

void outmsg_player_name(client *clt, int playerno, char *name);

void outmsg_game_scores(client *clt, int scorea, int scoreb);

void outmsg_start_timeout(client *clt, int timeout);

void outmsg_ball(client *clt, double xpos, double ypos, double xvel, double yvel);

void outmsg_paddle(client *clt, int player, double ypos, double yvel);

void outmsg_game_finished(client *clt, char *winner);

void outmsg_connection_error(client *clt, char *desc);

void outmsg_connection_restored(client *clt);

#endif /* UPS_SP_OUTMSG_H */
