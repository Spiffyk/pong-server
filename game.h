#ifndef UPS_SP_GAME_H
#define UPS_SP_GAME_H

#include <uuid/uuid.h>
#include <pthread.h>
#include "client.h"
#include "game_consts.h"
#include "conf.h"

typedef struct {
    pthread_mutex_t mutex;
    uuid_t uuid;
    char name[GAME_NAME_LENGTH];
    unsigned int state;
    int name_notified;
    int state_notified;

    client *playera;
    int playera_key;

    client *playerb;
    int playerb_key;

    pthread_t pthread;
} game;

typedef struct {
    game *gme;
    conf *config;
} game_main_args;


game *game_create(char *name);

void game_free(game **g);

void *game_main(void *arg);

int game_entry_fn(sdict_entry *entry);

int game_unentry_fn(sdict_entry *entry);

#endif /* UPS_SP_GAME_H */
