#ifndef UPS_SP_CONF_H
#define UPS_SP_CONF_H

#include "sdict/sdict.h"

typedef struct {
    int log_level;
    char log_file[512];

    char server_motd[512];
    int server_port;
    char server_address[128];
    int server_max_clients;

    double game_field_width;
    double game_field_height;
    double game_paddle_margin;
    double game_paddle_width;
    double game_paddle_height;
    double game_paddle_speed;
    double game_ball_size;
    double game_ball_speed;
    int game_win_score;
} conf;

conf *conf_create(void);

conf *conf_from_sdict(sdict *dict);

void conf_free(conf **c);

#endif /* UPS_SP_CONF_H */
