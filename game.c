#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include "logc/log.h"
#include "sdict/sdict.h"
#include "util.h"
#include "game.h"
#include "outmsg.h"

#define WAITING_SLEEP_TIME 250
#define GAME_LOOP_SLEEP_TIME 40
#define MAX_WAITING_TIME 180 * 1000

typedef struct {
    double ypos;
    double yvel;
} paddle;

typedef struct {
    double xpos;
    double ypos;
    double xvel;
    double yvel;
} ball;

game *game_create(char *name) {
    game *result;

    result = malloc(sizeof(game));
    if (!result) {
        return NULL;
    }

    pthread_mutex_init(&(result->mutex), NULL);
    uuid_generate_random(result->uuid);
    if (name) {
        strncpy(result->name, name, GAME_NAME_LENGTH);
    } else {
        result->name[0] = '\0';
    }
    result->state = GAME_STATE_AWAITING_PLAYER;
    result->name_notified = 0;
    result->state_notified = 0;

    result->playera = NULL;
    result->playera_key = (int) random();

    result->playerb = NULL;
    do { // make sure keys are not equal
        result->playerb_key = (int) random();
    } while (result->playera_key == result->playerb_key);

    return result;
}

void game_free(game **g) {
    if (!g || !(*g)) {
        return;
    }

    pthread_mutex_destroy(&((*g)->mutex));
    free(*g);
    *g = NULL;
}

void *game_main(void *arg) {
    game_main_args *args = arg;
    game *gme = args->gme;
    conf *config = args->config;

    double ball_isospeed = sqrt(2) * config->game_ball_speed;
    double ball_hsize = config->game_ball_size / 2;
    double paddle_speed = config->game_paddle_speed;
    double paddle_hheight = config->game_paddle_height / 2;
    double field_xlimit = config->game_field_width / 2;
    double field_ylimit = config->game_field_height / 2;
    double field_plimit = field_xlimit - config->game_paddle_margin - config->game_paddle_width;

    inmsg *msg;
    inmsg_game_paddle *msg_paddle;
    int waiting_for_players = 1;
    int game_running = 1;
    int was_paused = 0;
    unsigned long ts_last, ts, delta_l, actual_update_ts;
    double delta;
    int sleep_time;

    int scorea, scoreb;
    int timeout;
    long timeout_delta;
    paddle pa, pb;
    ball b;

    free(args);
    log_info("Starting thread for game '%s'", gme->name);

    while(waiting_for_players) {
        usleep(WAITING_SLEEP_TIME * MILLIS_TO_MICROS);
        pthread_mutex_lock(&gme->mutex);
        waiting_for_players = !gme->playera || !gme->playerb || gme->state == GAME_STATE_AWAITING_PLAYER;

        if (gme->playera) {
            pthread_mutex_lock(&gme->playera->mutex);
            if (gme->playera->client_state == CLIENT_STATE_LEAVING_GAME) {
                log_info("Client %s has requested to leave game '%s'.", client_logid(gme->playera), gme->name);
                game_running = 0;
                waiting_for_players = 0;
            }
            pthread_mutex_unlock(&gme->playera->mutex);
        }

        pthread_mutex_unlock(&gme->mutex);
    }

    scorea = 0;
    scoreb = 0;
    timeout = 4;
    timeout_delta = 0;
    pa.ypos = 0;
    pa.yvel = 0;
    pb.ypos = 0;
    pb.yvel = 0;
    b.xpos = 0;
    b.xvel = -ball_isospeed;
    b.ypos = 0;
    b.yvel = ball_isospeed;

    if (game_running) {
        pthread_mutex_lock(&gme->mutex);
        pthread_mutex_lock(&gme->playera->mutex);
        pthread_mutex_lock(&gme->playerb->mutex);

        outmsg_game_meta(
                gme->playera,
                config->game_field_width,
                config->game_field_height,
                config->game_ball_size,
                config->game_paddle_width,
                config->game_paddle_height,
                config->game_paddle_margin,
                config->game_win_score);
        outmsg_game_meta(
                gme->playerb,
                config->game_field_width,
                config->game_field_height,
                config->game_ball_size,
                config->game_paddle_width,
                config->game_paddle_height,
                config->game_paddle_margin,
                config->game_win_score);

        outmsg_game_scores(gme->playera, scorea, scoreb);
        outmsg_game_scores(gme->playerb, scorea, scoreb);

        outmsg_player_name(gme->playera, 0, gme->playera->name);
        outmsg_player_name(gme->playera, 1, gme->playerb->name);
        outmsg_player_name(gme->playerb, 0, gme->playera->name);
        outmsg_player_name(gme->playerb, 1, gme->playerb->name);

        pthread_mutex_unlock(&gme->playera->mutex);
        pthread_mutex_unlock(&gme->playerb->mutex);
        pthread_mutex_unlock(&gme->mutex);
    }

    log_debug("Gonna start game");
    actual_update_ts = ts = mono_msec();
    while(game_running) {
        ts_last = ts;
        ts = mono_msec();
        delta_l = ts - ts_last;
        delta = delta_l / 1000.0;

        pthread_mutex_lock(&gme->mutex);
        pthread_mutex_lock(&gme->playera->mutex);
        pthread_mutex_lock(&gme->playerb->mutex);

        if (!gme->playera || (gme->playera->connection_state & CONNECTION_STATE_TYPE_MASK) == CONNECTION_STATE_TYPE_PROBLEMATIC) {
            if (!was_paused) {
                was_paused = 1;
                outmsg_connection_error(gme->playerb, "The other player is having connection problems!");
            }
			timeout = 4;
            goto loop_end;
        }

        if (!gme->playerb || (gme->playerb->connection_state & CONNECTION_STATE_TYPE_MASK) == CONNECTION_STATE_TYPE_PROBLEMATIC) {
            if (!was_paused) {
                was_paused = 1;
                outmsg_connection_error(gme->playera, "The other player is having connection problems!");
            }
			timeout = 4;
            goto loop_end;
        }

        if (was_paused) {
            was_paused = 0;
            outmsg_connection_restored(gme->playera);
            outmsg_connection_restored(gme->playerb);
        }

        if (ts - actual_update_ts > MAX_WAITING_TIME) {
            log_info("Game '%s' has timed out and will be terminated.", gme->name);
            game_running = 0;
            goto loop_end;
        }

        if (gme->playera->client_state == CLIENT_STATE_LEAVING_GAME) {
            log_info("Client %s has requested to leave game '%s'.", client_logid(gme->playera), gme->name);
            outmsg_sys_error(gme->playerb, "Game over, the other player has left");
            game_running = 0;
            goto loop_end;
        }

        if (gme->playerb->client_state == CLIENT_STATE_LEAVING_GAME) {
            log_info("Client %s has requested to leave game '%s'.", client_logid(gme->playerb), gme->name);
            outmsg_sys_error(gme->playera, "Game over, the other player has left");
            game_running = 0;
            goto loop_end;
        }

        if (timeout > 0) {
            timeout_delta += delta_l;

            if (timeout_delta >= 1000) {
                timeout -= timeout_delta / 1000;
                timeout_delta %= 1000;
                if (timeout_delta < 0) {
                    timeout_delta = 0;
                }
                outmsg_start_timeout(gme->playera, timeout);
                outmsg_start_timeout(gme->playerb, timeout);

                if (timeout == 0) {
                    outmsg_ball(gme->playera, b.xpos, b.ypos, b.xvel, b.yvel);
                    outmsg_ball(gme->playerb, b.xpos, b.ypos, b.xvel, b.yvel);
                }
            }

            goto loop_end;
        }

        // Process left player's input
        while(gme->playera->in_queue->head) {
            msg = cmq_pop(gme->playera->in_queue);
            if (msg->type == INMSG_GAME_PADDLE) {
                msg_paddle = (inmsg_game_paddle *) msg;

                pa.ypos = msg_paddle->ypos;
                if (msg_paddle->yvel > 0) {
                    pa.yvel = paddle_speed;
                } else if (msg_paddle->yvel < 0) {
                    pa.yvel = -paddle_speed;
                } else {
                    pa.yvel = 0;
                }

                outmsg_paddle(gme->playera, 0, pa.ypos, pa.yvel);
                outmsg_paddle(gme->playerb, 0, pa.ypos, pa.yvel);
            }
        }

        // Process right player's input
        while(gme->playerb->in_queue->head) {
            msg = cmq_pop(gme->playerb->in_queue);
            if (msg->type == INMSG_GAME_PADDLE) {
                msg_paddle = (inmsg_game_paddle *) msg;

                pb.ypos = msg_paddle->ypos;
                if (msg_paddle->yvel > 0) {
                    pb.yvel = paddle_speed;
                } else if (msg_paddle->yvel < 0) {
                    pb.yvel = -paddle_speed;
                } else {
                    pb.yvel = 0;
                }

                outmsg_paddle(gme->playera, 1, pb.ypos, pb.yvel);
                outmsg_paddle(gme->playerb, 1, pb.ypos, pb.yvel);
            }
        }

        // Process movement
        pa.ypos += pa.yvel * delta;
        pb.ypos += pb.yvel * delta;
        b.xpos += b.xvel * delta;
        b.ypos += b.yvel * delta;

        if ((pa.ypos + paddle_hheight) > field_ylimit) {
            pa.ypos = field_ylimit - paddle_hheight;
            pa.yvel = 0;
            outmsg_paddle(gme->playera, 0, pa.ypos, pa.yvel);
            outmsg_paddle(gme->playerb, 0, pa.ypos, pa.yvel);
        }

        if ((pa.ypos - paddle_hheight) < -field_ylimit) {
            pa.ypos = paddle_hheight - field_ylimit;
            pa.yvel = 0;
            outmsg_paddle(gme->playera, 0, pa.ypos, pa.yvel);
            outmsg_paddle(gme->playerb, 0, pa.ypos, pa.yvel);
        }

        if ((pb.ypos + paddle_hheight) > field_ylimit) {
            pb.ypos = field_ylimit - paddle_hheight;
            pb.yvel = 0;
            outmsg_paddle(gme->playera, 1, pb.ypos, pb.yvel);
            outmsg_paddle(gme->playerb, 1, pb.ypos, pb.yvel);
        }

        if ((pb.ypos - paddle_hheight) < -field_ylimit) {
            pb.ypos = paddle_hheight - field_ylimit;
            pb.yvel = 0;
            outmsg_paddle(gme->playera, 1, pb.ypos, pb.yvel);
            outmsg_paddle(gme->playerb, 1, pb.ypos, pb.yvel);
        }

        // Top border bounce
        if ((b.ypos - ball_hsize) < -field_ylimit) {
            b.ypos = ball_hsize - field_ylimit;
            b.yvel = -b.yvel;
            outmsg_ball(gme->playera, b.xpos, b.ypos, b.xvel, b.yvel);
            outmsg_ball(gme->playerb, b.xpos, b.ypos, b.xvel, b.yvel);
        }

        // Bottom border bounce
        if ((b.ypos + ball_hsize) > field_ylimit) {
            b.ypos = field_ylimit - ball_hsize;
            b.yvel = -b.yvel;
            outmsg_ball(gme->playera, b.xpos, b.ypos, b.xvel, b.yvel);
            outmsg_ball(gme->playerb, b.xpos, b.ypos, b.xvel, b.yvel);
        }

        // Left player bounce
        if ((b.xpos - ball_hsize) < -field_plimit &&
            (b.ypos - ball_hsize) > (pa.ypos - paddle_hheight) &&
            (b.ypos + ball_hsize) < (pa.ypos + paddle_hheight)) {

            b.xvel = -b.xvel;
            b.xpos = ball_hsize - field_plimit;
            outmsg_ball(gme->playera, b.xpos, b.ypos, b.xvel, b.yvel);
            outmsg_ball(gme->playerb, b.xpos, b.ypos, b.xvel, b.yvel);
        }

        // Right player bounce
        if ((b.xpos + ball_hsize) > field_plimit &&
            (b.ypos - ball_hsize) > (pb.ypos - paddle_hheight) &&
            (b.ypos + ball_hsize) < (pb.ypos + paddle_hheight)) {

            b.xvel = -b.xvel;
            b.xpos = field_plimit - ball_hsize;
            outmsg_ball(gme->playera, b.xpos, b.ypos, b.xvel, b.yvel);
            outmsg_ball(gme->playerb, b.xpos, b.ypos, b.xvel, b.yvel);
        }

        // Left border score
        if ((b.xpos - ball_hsize) < -field_xlimit) {
            scoreb++;
            b.xpos = 0;
            b.ypos = 0;
            outmsg_game_scores(gme->playera, scorea, scoreb);
            outmsg_game_scores(gme->playerb, scorea, scoreb);
            outmsg_ball(gme->playera, 0, 0, 0, 0);
            outmsg_ball(gme->playerb, 0, 0, 0, 0);
            timeout = 4;
        }

        // Right border score
        if ((b.xpos + ball_hsize) > field_xlimit) {
            scorea++;
            b.xpos = 0;
            b.ypos = 0;
            outmsg_game_scores(gme->playera, scorea, scoreb);
            outmsg_game_scores(gme->playerb, scorea, scoreb);
            outmsg_ball(gme->playera, 0, 0, 0, 0);
            outmsg_ball(gme->playerb, 0, 0, 0, 0);
            timeout = 4;
        }

        // Game end
        if (scorea >= config->game_win_score) {
            outmsg_game_finished(gme->playera, gme->playera->name);
            outmsg_game_finished(gme->playerb, gme->playera->name);
            game_running = 0;
        } else if (scoreb >= config->game_win_score) {
            outmsg_game_finished(gme->playera, gme->playerb->name);
            outmsg_game_finished(gme->playerb, gme->playerb->name);
            game_running = 0;
        }

        actual_update_ts = mono_msec();

        loop_end:
        pthread_mutex_unlock(&gme->playera->mutex);
        pthread_mutex_unlock(&gme->playerb->mutex);
        pthread_mutex_unlock(&gme->mutex);

        sleep_time = (int) GAME_LOOP_SLEEP_TIME - (int) delta_l;
        if (sleep_time < 0) {
            sleep_time = 0;
        }
        usleep((useconds_t) (sleep_time) * MILLIS_TO_MICROS);
    }

    pthread_mutex_lock(&gme->mutex);
    gme->state = GAME_STATE_FINAL;
    gme->state_notified = 0;
    log_info("Ending game '%s'", gme->name);
    pthread_mutex_unlock(&gme->mutex);

    return NULL;
}

int game_entry_fn(sdict_entry *entry) {
    entry->key = ((game *) entry->value)->uuid;
    return 0;
}

int game_unentry_fn(sdict_entry *entry) {
    game_free((game **) &(entry->value));
    return 0;
}
