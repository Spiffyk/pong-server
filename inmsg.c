#include <stdlib.h>
#include <pthread.h>
#include <ctype.h>
#include <string.h>

#include "logc/log.h"

#include "exit_codes.h"
#include "util.h"
#include "sdict/sdict.h"
#include "inmsg.h"

#define MSG_BUFFER_SIZE 2048
#define INMSG_BUFFER_SIZE 128

typedef struct {
    inmsg *(*fn)(char *);
} parse_fn_holder;

int parser_entry_fn(sdict_entry *entry);

inmsg *parse_inmsg_sys_login(char *message);

inmsg *parse_inmsg_sys_logout(char *message);

inmsg *parse_inmsg_sys_pong(char *message);

inmsg *parse_inmsg_lobby_game_create(char *message);

inmsg *parse_inmsg_lobby_join(char *message);

inmsg *parse_inmsg_lobby_rejoin(char *message);

inmsg *parse_inmsg_game_leave(char *message);

inmsg *parse_inmsg_game_paddle(char *message);

char *inmsg_get_string_word(char **message, size_t limit);

char *inmsg_get_string_rest(char **message, size_t limit);

long inmsg_get_integer(char **message);

double inmsg_get_decimal(char **message);

void prepare_parser_sdict(void);

void cleanup_parser_sdict(void);

sdict *parser_sdict = NULL;
char strbuffer[MSG_BUFFER_SIZE] = {0};
char msgbuffer[INMSG_BUFFER_SIZE];

int parser_entry_fn(sdict_entry *entry) {
    int err;
    parse_fn_holder *holder;

    err = string_key_entry_fn(entry);
    if (err) {
        return err;
    }

    holder = malloc(sizeof(parse_fn_holder));
    if (!holder) {
        free(entry->key);
        return SDICT_ERR_MALLOC_FAILURE;
    }

    *holder = *((parse_fn_holder *) entry->value);
    entry->value = holder;

    return 0;
}

inmsg *parse_inmsg_sys_login(char *message) {
    inmsg_sys_login *result = (inmsg_sys_login *) msgbuffer;
    char *ptr;
    result->meta.type = INMSG_SYS_LOGIN;
    result->meta.size = sizeof(inmsg_sys_login);
    ptr = inmsg_get_string_rest(&message, sizeof(result->name));
    if (!ptr) {
        return NULL;
    }
    strncpy(result->name, ptr, sizeof(result->name));
    return (inmsg *) result;
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic push
inmsg *parse_inmsg_sys_logout(char *message) {
    inmsg *result = (inmsg *) msgbuffer;
    result->type = INMSG_SYS_LOGOUT;
    result->size = sizeof(inmsg);
    return result;
}
#pragma GCC diagnostic pop

inmsg *parse_inmsg_sys_pong(char *message) {
    inmsg_sys_pong *result = (inmsg_sys_pong *) msgbuffer;
    char *ptr;
    result->meta.type = INMSG_SYS_PONG;
    result->meta.size = sizeof(inmsg_sys_pong);
    ptr = inmsg_get_string_word(&message, UUID_STR_LEN);
    if (!ptr) {
        return NULL;
    }
    uuid_parse(ptr, result->uuid);
    return (inmsg *) result;
}

inmsg *parse_inmsg_lobby_game_create(char *message) {
    inmsg_lobby_game_create *result = malloc(sizeof(inmsg_lobby_game_create));
    char *ptr;
    result->meta.type = INMSG_LOBBY_GAME_CREATE;
    ptr = inmsg_get_string_rest(&message, sizeof(result->name));
    if (!ptr) {
        return NULL;
    }
    strncpy(result->name, ptr, sizeof(result->name));
    return (inmsg *) result;
}

inmsg *parse_inmsg_lobby_join(char *message) {
    char *ptr;
    int err;
    inmsg_lobby_join *result = (inmsg_lobby_join *) msgbuffer;
    result->meta.type = INMSG_LOBBY_JOIN;
    result->meta.size = sizeof(inmsg_lobby_join);
    ptr = inmsg_get_string_word(&message, UUID_STR_LEN);
    if (!ptr) {
        return NULL;
    }
    err = uuid_parse(ptr, result->uuid);
    if (err) {
        return NULL;
    }
    return (inmsg *) result;
}

inmsg *parse_inmsg_lobby_rejoin(char *message) {
    inmsg_lobby_rejoin *result = (inmsg_lobby_rejoin *) msgbuffer;
    char *ptr;
    int err;
    result->meta.type = INMSG_LOBBY_REJOIN;
    result->meta.size = sizeof(inmsg_lobby_rejoin);
    ptr = inmsg_get_string_word(&message, UUID_STR_LEN);
    if (!ptr) {
        return NULL;
    }
    err = uuid_parse(ptr, result->uuid);
    if (err) {
        return NULL;
    }
    result->key = (int) inmsg_get_integer(&message);
    return (inmsg *) result;
}

inmsg *parse_inmsg_game_leave(char *message) {
    inmsg *result = (inmsg *) msgbuffer;
    result->type = INMSG_GAME_LEAVE;
    result->size = sizeof(inmsg);
    return result;
}

inmsg *parse_inmsg_game_paddle(char *message) {
    inmsg_game_paddle *result = (inmsg_game_paddle *) msgbuffer;
    result->meta.type = INMSG_GAME_PADDLE;
    result->meta.size = sizeof(inmsg_game_paddle);
    result->ypos = inmsg_get_decimal(&message);
    result->yvel = (int) inmsg_get_integer(&message);
    return (inmsg *) result;
}

void prepare_parser_sdict(void) {
    parse_fn_holder holder;

    parser_sdict = sdict_create(
            string_djb2_hashcode, string_key_equals, parser_entry_fn, base_unentry_fn, SDICT_DEFAULT_SIZE);
    if (!parser_sdict) {
        log_fatal("Could not lazy-init parser dictionary!");
        exit(ERR_SYS_ALLOC);
    }

    holder.fn = parse_inmsg_sys_login;
    sdict_put(parser_sdict, INMSG_SYS_LOGIN_NAME, &holder);

    holder.fn = parse_inmsg_sys_logout;
    sdict_put(parser_sdict, INMSG_SYS_LOGOUT_NAME, &holder);

    holder.fn = parse_inmsg_sys_pong;
    sdict_put(parser_sdict, INMSG_SYS_PONG_NAME, &holder);

    holder.fn = parse_inmsg_lobby_game_create;
    sdict_put(parser_sdict, INMSG_LOBBY_GAME_CREATE_NAME, &holder);

    holder.fn = parse_inmsg_lobby_join;
    sdict_put(parser_sdict, INMSG_LOBBY_JOIN_NAME, &holder);

    holder.fn = parse_inmsg_lobby_rejoin;
    sdict_put(parser_sdict, INMSG_LOBBY_REJOIN_NAME, &holder);

    holder.fn = parse_inmsg_game_leave;
    sdict_put(parser_sdict, INMSG_GAME_LEAVE_NAME, &holder);

    holder.fn = parse_inmsg_game_paddle;
    sdict_put(parser_sdict, INMSG_GAME_PADDLE_NAME, &holder);
}

void cleanup_parser_sdict(void) {
    sdict_free(&parser_sdict);
}

inmsg *parse_message(char *message) {
    parse_fn_holder *holder;
    char message_name[1024] = {0};
    char *reader_c, *result_c;
    inmsg *result;

    // lazy-init parser dictionary
    if (!parser_sdict) {
        prepare_parser_sdict();
        atexit(cleanup_parser_sdict);
    }

    for (reader_c = message, result_c = message_name; *reader_c != ' ' && *reader_c != '\0'; reader_c++, result_c++) {
        *result_c = *reader_c;
    }

    if (*reader_c != '\0') {
        reader_c++;
    }

    holder = (parse_fn_holder *) sdict_get(parser_sdict, message_name);
    if (!holder) {
        return NULL;
    }

    result = holder->fn(reader_c);
    if (result) {
        result->msec = mono_msec();
    }
    return result;
}

char *inmsg_get_string_word(char **message, size_t limit) {
    char *ptr;
    unsigned int counter;

    for (ptr = strbuffer, counter = 0; **message && !isspace(**message) && counter < MSG_BUFFER_SIZE - 1; ptr++, counter++, (*message)++) {
        *ptr = **message;
    }

    if (limit && counter >= limit) {
        return NULL;
    }

    strbuffer[counter] = '\0';
    (*message)++;
    return strbuffer;
}

char *inmsg_get_string_rest(char **message, size_t limit) {
    char *ptr;
    unsigned int counter;

    for (ptr = strbuffer, counter = 0; **message && counter < MSG_BUFFER_SIZE - 1; ptr++, counter++, (*message)++) {
        *ptr = **message;
    }

    if (limit && counter >= limit) {
        return NULL;
    }

    strbuffer[counter] = '\0';
    (*message)++;
    return strbuffer;
}

long inmsg_get_integer(char **message) {
    inmsg_get_string_word(message, 0);
    return atoi(strbuffer);
}

double inmsg_get_decimal(char **message) {
    inmsg_get_string_word(message, 0);
    return atof(strbuffer);
}
