#ifndef UPS_SP_MSGPROC_H
#define UPS_SP_MSGPROC_H

#include <pthread.h>
#include "conf.h"
#include "sdict/sdict.h"

#define cmq_isempty(queue) (!(queue->head))

int server_main(conf *config, int srv_sock);

#endif /* UPS_SP_MSGPROC_H */
