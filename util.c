#include <time.h>
#include <stdlib.h>
#include <uuid/uuid.h>
#include <ctype.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include "util.h"

unsigned long mono_msec(void) {
    struct timespec ts;

    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ((unsigned long) ts.tv_sec * 1000) + ((unsigned long) ts.tv_nsec / 1000000);
}

unsigned long intkey_hash(void *i) {
    return (unsigned long) *((int *) i);
}

int intkey_equals(void *a, void *b) {
    return *((int *) a) == *((int *) b);
}

unsigned long uuid_hash(void *uuid) {
    return *((unsigned long *) uuid) ^ *((unsigned long *) uuid + sizeof(unsigned long));
}

int uuid_equals(void *uua, void *uub) {
    unsigned int i;
    for (i = 0; i < sizeof(uuid_t); i++) {
        if (((char *) uua)[i] != ((char *) uub)[i]) {
            return 0;
        }
    }
    return 1;
}

int isalnumorspace(int c) {
    return (isalnum(c) || c == ' ');
}

int strchk(char *str, int (*chkfn)(int), size_t limit) {
    unsigned long i;

    for (i = 0; (!limit || i < limit) && str[i]; i++) {
        if (!chkfn(str[i])) {
            return 0;
        }
    }

    return 1;
}

int fd_valid(int fd) {
    return fcntl(fd, F_GETFL) != -1 || errno != EBADF;
}
