#include <errno.h>
#include <getopt.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <libnet.h>

#include "logc/log.h"
#include "sdict/sdict.h"
#include "sdict/sdict_file.h"

#include "conf.h"
#include "server.h"
#include "exit_codes.h"

#define CONFIG_FILE "server.conf"

typedef struct {
    char *config_path;
} opts;

int fill_opts(int argc, char *argv[], opts *options);
int fill_sockaddr(conf *config, struct sockaddr_in *srv_addr);
int main(int argc, char *argv[]);

static struct option long_options[] =
        {
                {"config", required_argument, NULL, 'c'},
                {NULL, 0, NULL, 0}
        };

int fill_opts(int argc, char *argv[], opts *options) {
    int opt;

    options->config_path = CONFIG_FILE;

    while ((opt = getopt_long(argc, argv, "c:", long_options, NULL)) != -1) {
        switch(opt) {
            case 'c':
                options->config_path = optarg;
                break;
            default:
                break;
        }
    }

    return 0;
}

int fill_sockaddr(conf *config, struct sockaddr_in *srv_addr) {
    int aton_success;

    memset(srv_addr, 0, sizeof(struct sockaddr_in));
    srv_addr->sin_family = AF_INET;

    if (strcmp(config->server_address, "ANY") == 0) {
        srv_addr->sin_addr.s_addr = INADDR_ANY;
    } else {
        aton_success = inet_aton(config->server_address, &(srv_addr->sin_addr));
        if (!aton_success) {
            log_error("Invalid server address '%s'", config->server_address);
            return EXIT_FAILURE;
        }
    }

    srv_addr->sin_port = htons((unsigned short) config->server_port);

    return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
    opts options;
    sdict *config_dict;
    conf *config;

    int srv_sock;
    struct sockaddr_in srv_addr;
    int param;
    int err;
    int main_result = EXIT_SUCCESS;
    FILE *logfile = NULL;

    fill_opts(argc, argv, &options);

    /* Read configuration file */
    config_dict = sdict_load(options.config_path);
    err = sdict_error();
    if (err) {
        if (err & SDICT_FILE_ERROR_MASK) {
            fprintf(stderr, "Could not read configuration file '%s'. Error code 0x%04X (%u)\n",
                    options.config_path, err, err);
        } else {
            fprintf(stderr, "Could not store configuration in memory. Error code 0x%04X (%u)\n", err, err);
        }
        main_result = ERR_CONFIG_READ;
        goto exit;
    }
    config = conf_from_sdict(config_dict);
    sdict_free(&config_dict);

    /* Set up logging */
    log_set_level(config->log_level);
    if (config->log_file[0]) {
        logfile = fopen(config->log_file, "a");
        if (logfile) {
            log_set_fp(logfile);
        } else {
            log_warn("Could not open file \"%s\" for logging (errno: %d)", config->log_file, errno);
        }
    }

    log_info("Starting Pong Server");

    /* Set up network communication */
    srv_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (srv_sock == -1) {
        log_fatal("Socket error (%d)", errno);
        main_result = ERR_SRV_SOCKET;
        goto exit_cleanup_config;
    }
    log_info("Socket created");

    err = fill_sockaddr(config, &srv_addr);
    if (err) {
        log_fatal("Bad address/port configuration");
        main_result = ERR_CONFIG_ADDR;
        goto exit_cleanup_sock;
    }

    param = 1;
    err = setsockopt(srv_sock, SOL_SOCKET, SO_REUSEADDR, &param, sizeof(int));
    if (err == -1) {
        log_fatal("Reuseaddr sockopt error (errno: %d)", errno);
        main_result = ERR_SRV_SOCKOPT;
        goto exit_cleanup_sock;
    }

    err = bind(srv_sock, (struct sockaddr *) &srv_addr, sizeof(struct sockaddr_in));
    if (err) {
        log_fatal("Bind error (errno: %d)", errno);
        main_result = ERR_SRV_BIND;
        goto exit_cleanup_sock;
    }

    err = listen(srv_sock, 5);
    if (err) {
        log_fatal("Listen error (errno: %d)", errno);
        main_result = ERR_SRV_LISTEN;
        goto exit_cleanup_sock;
    }

    if (srv_addr.sin_addr.s_addr == 0) {
        log_info("Listening on port %d", ntohs(srv_addr.sin_port));
    } else {
        log_info("Listening on %s:%d", inet_ntoa(srv_addr.sin_addr), ntohs(srv_addr.sin_port));
    }

    /* Print message of the day */
    if (config->server_motd[0]) {
        log_info("Server's MotD: \"%s\"", config->server_motd);
    }

    /* Start processing messages */
    err = server_main(config, srv_sock);
    if (err) {
        log_fatal("Server error 0x%X", err);
        main_result = err;
    }

    exit_cleanup_sock:
    close(srv_sock);

    exit_cleanup_config:
    conf_free(&config);

    exit:
    log_info("Exiting with code 0x%04X", main_result);
    if (logfile) {
        fputc('\n', logfile);
        fclose(logfile);
    }
    return main_result;
}
