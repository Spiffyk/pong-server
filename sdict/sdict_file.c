#include <fcntl.h>
#include <ctype.h>

#include "sdict_file.h"

#define BUF_SIZE 8192

sdict * sdict_read(FILE *file);

int file_error = 0;

sdict * sdict_read(FILE *file) {
    char buffer[BUF_SIZE] = {0};
    char *buf_ptr;
    char *key_ptr = NULL;
    char *value_ptr = NULL;
    int c;
    sdict *result = sdict_string_create(SDICT_DEFAULT_SIZE);

    if (sdict_error()) {
        if (result) {
            sdict_free_noreset(&result);
        }
        return NULL;
    }

    file_error = 0;

    while (!feof(file)) {
        buf_ptr = buffer;

        // skip whitespace
        while (isspace(c = getc(file)));

        if (c == EOF) {
            goto finish;
        }

        if (c == '=' || c == '\n') {
            sdict_free_noreset(&result);
            sdict_set_error(SDICT_ERR_INVALID_KEY);
            return NULL;
        }

        // read key into buffer
        key_ptr = buf_ptr;
        while (c != '=') {
            if (buf_ptr - buffer >= BUF_SIZE) {
                sdict_free_noreset(&result);
                sdict_set_error(SDICT_ERR_BUFFER_OVERFLOW);
                return NULL;
            }

            if (c == EOF) {
                sdict_free_noreset(&result);
                sdict_set_error(SDICT_ERR_PREMAT_EOF);
                return NULL;
            }

            if ((c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && c != '.' && c != '_') {
                while (isspace(c = getc(file)));

                if (c == '=') {
                    break;
                }

                sdict_free_noreset(&result);
                sdict_set_error(SDICT_ERR_INVALID_KEY);
                return NULL;
            }

            *buf_ptr = (char) c;
            buf_ptr++;
            c = getc(file);
        }

        // NULL-terminate
        *buf_ptr = 0;
        buf_ptr++;

        // skip whitespace
        while (isspace(c = getc(file)));
        if (c == EOF) {
            sdict_free_noreset(&result);
            sdict_set_error(SDICT_ERR_PREMAT_EOF);
            return NULL;
        }

        // read value into buffer
        value_ptr = buf_ptr;
        while (c != '\n' && c != EOF) {
            if (buf_ptr - buffer >= BUF_SIZE) {
                sdict_free_noreset(&result);
                sdict_set_error(SDICT_ERR_BUFFER_OVERFLOW);
                return NULL;
            }

            *buf_ptr = (char) c;
            buf_ptr++;
            c = getc(file);
        }

        *buf_ptr = 0;

        sdict_put(result, key_ptr, value_ptr);
        if (sdict_error()) {
            sdict_free_noreset(&result);
            return NULL;
        }
    }

    finish:
    return result;
}


sdict * sdict_load(char *path) {
    sdict *result;
    FILE *file = fopen(path, "r");

    if (!file) {
        sdict_set_error(SDICT_ERR_CANNOT_OPEN);
        return NULL;
    }

    result = sdict_read(file);

    if (fclose(file)) {
        sdict_free_noreset(&result);
        sdict_set_error(SDICT_ERR_IO);
        file_error = ferror(file);
        return NULL;
    }

    return result;
}

int sdict_file_error(void) {
    return file_error;
}
