#ifndef UPS_SP_SDICT_H
#define UPS_SP_SDICT_H

#include <pthread.h>

/**
 * This error code is set when no error has happened during the last sdict operation.
 */
#define SDICT_ERR_NONE 0x0000

/**
 * This error code is set when sdict failed to allocate memory during its last operation.
 */
#define SDICT_ERR_MALLOC_FAILURE 0x0001

/**
 * This error code is set when an entry function fails.
 */
#define SDICT_ERR_ENTRY_FN 0x0002

/**
 * This error code is set when the mutex of the sdict could not be initialized.
 */
#define SDICT_ERR_MUTEX_FAILURE 0x0003

/**
 * This error code is set when a function is passed a null pointer.
 */
#define SDICT_ERR_NULL 0x0301

/**
 * The default length of a hash table.
 */
#define SDICT_DEFAULT_SIZE 256


/**
 * Resets error code to <code>SDICT_ERR_NONE</code> (zero). To be used by sdict extensions, not by the user; all sdict
 * functions are supposed to reset the error code automatically.
 */
#define sdict_reset_error() sdict_set_error(SDICT_ERR_NONE)

/**
 * An entry in the hash table.
 */
typedef struct _sdict_entry {
    unsigned long hash;
    void *key;
    void *value;
    struct _sdict_entry *next;
} sdict_entry;

/**
 * A dictionary structure.
 */
typedef struct {
    pthread_mutex_t mutex;
    unsigned long (*hash_fn)(void *);
    int (*key_equals)(void *, void *);
    int (*entry_fn)(sdict_entry *);
    int (*unentry_fn)(sdict_entry *);
    unsigned int size;
    unsigned int table_length;
    sdict_entry **table;
} sdict;

/**
 * An iterator over a dictionary.
 */
typedef struct {
    sdict *dict;
    long index;
    sdict_entry dummy;
    sdict_entry *current;
} sdict_iterator;

/**
 * Calculates a hashcode for the specified string.
 *
 * @param key the string to calculate a hashcode for
 * @return the hashcode of the specified string
 */
unsigned long string_djb2_hashcode(void *key);

/**
 * String comparing function for use in a dictionary.
 */
int string_key_equals(void *s1, void *s2);

/**
 * Utility function for handling sdict entry creation with string keys.
 *
 * @param entry
 * @return
 */
int string_key_entry_fn(sdict_entry *entry);

/**
 * Utility function for handling sdict entry cleanup with string keys.
 *
 * @param entry
 * @return
 */
int string_key_unentry_fn(sdict_entry *entry);

/**
 * Basic sdict entry cleanup function. Simply calls <code>free</code> on key and value.
 *
 * @param entry
 * @return
 */
int base_unentry_fn(sdict_entry *entry);

/**
 * Creates a new string dictionary with the given table length. The length signifies the number of individual hash
 * table entry points. Each entry point then contains a linked list of entries.
 *
 * @param table_length the length of the table
 * @return a newly created string dictionary or <code>NULL</code> on failure
 */
sdict * sdict_string_create(unsigned int table_length);

/**
 * Creates a new dictionary.
 *
 * @param hash_fn key hashing function - generates a hash from the key
 * @param key_equals determines whether two keys are equal
 * @param entry_fn transforms the given entry (if needed; may be NULL)
 * @param unentry_fn cleans up after transform (if needed; may be NULL)
 * @param table_length the length of the table
 * @return a newly created string dictionary or <code>NULL</code> on failure
 */
sdict * sdict_create(
        unsigned long (*hash_fn)(void *),
        int (*key_equals)(void *, void *),
        int (*entry_fn)(sdict_entry *),
        int (*unentry_fn)(sdict_entry *),
        unsigned int table_length);

/**
 * Frees the specified dictionary, setting the reference to <code>NULL</code>.
 *
 * @param dict a reference to a dictionary
 */
void sdict_free(sdict **dict);

/**
 * Frees the specified dictionary, setting the reference to <code>NULL</code>, without resetting the error code.
 * For use by extensions.
 *
 * @param dict
 */
void sdict_free_noreset(sdict **dict);

/**
 * Puts an entry in the specified dictionary. If another entry with the same key already exists in the dictionary,
 * it is replaced with the new one. Internal copies of the key and value are created so that if the original ones are
 * modified or freed, the integrity of the dictionary remains intact.
 *
 * @param dict the dictionary to put the new entry in
 * @param key the key of the entry
 * @param value the value of the entry
 */
void sdict_put(sdict *dict, void *key, void *value);

/**
 * Removes the entry with the specified key from the specified dictionary. Does nothing if such key does not exist
 * in the dictionary.
 *
 * @param dict the dictionary to remove the new entry from
 * @param key the key of the removed entry
 * @return <code>1</code> if an entry with the key existed and was deleted, <code>0</code> if the entry did not exist
 */
int sdict_remove(sdict *dict, void *key);

/**
 * Gets a value from the specified dictionary by the specified key. If no entry with the key exists, <code>NULL</code>
 * is returned.
 *
 * @param dict the dictionary to retrieve the value from
 * @param key the key of the entry
 * @return the value string of the entry with the specified key or <code>NULL</code> if no such entry exists
 */
void * sdict_get(sdict *dict, void *key);

/**
 * Prints the contents of the hash table into <code>stdout</code>. Empty entry points are omitted.
 *
 * @param dict the dictionary to dump
 */
void sdict_dump(sdict *dict);

/**
 * Gets the last error code.
 *
 * @return the last error code
 */
unsigned int sdict_error(void);

/**
 * Sets the error code externally. To be used by sdict extensions, not by the user.
 *
 * @param err the error code to set
 */
void sdict_set_error(unsigned int err);

/**
 * Initializes an iterator over the specified dictionary.
 *
 * <strong>Note:</strong> the dictionary is locked until the iterator is unlocked with <code>sdict_it_unlock</code>
 *
 * @param dict the dictionary to iterate over
 * @param it the iterator structure to initialize and lock
 * @return the iterator
 */
void sdict_it(sdict *dict, sdict_iterator *it);

/**
 * Frees the specified iterator.
 *
 * @param it the iterator to free
 */
void sdict_it_unlock(sdict_iterator *it);

/**
 * Iterates over the specified iterator's dictionary.
 *
 * @param it the iterator to iterate with
 */
void sdict_it_next(sdict_iterator *it);

/**
 * Removes the current item the iterator is pointing at.
 *
 * @param it the iterator to iterate with
 */
void sdict_it_remove(sdict_iterator *it);

#endif /* UPS_SP_SDICT_H */
