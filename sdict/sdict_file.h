#ifndef UPS_SP_PROPS_LOADER_H
#define UPS_SP_PROPS_LOADER_H

#include <stdio.h>

#include "sdict.h"

#define SDICT_FILE_ERROR_MASK 0x0100
#define SDICT_ERR_IO 0x0101
#define SDICT_ERR_CANNOT_OPEN 0x0102
#define SDICT_ERR_BUFFER_OVERFLOW 0x0103

#define SDICT_ERR_PREMAT_EOF 0x0111
#define SDICT_ERR_INVALID_KEY 0x0112

/**
 * Reads a string dictionary from the file specified by the provided path. It is up to the user to free this dictionary.
 *
 * @param path the path to the file to read from
 * @return a string dictionary read from the file
 */
sdict * sdict_load(char *path);

int sdict_file_error(void);

#endif /* UPS_SP_PROPS_LOADER_H */
