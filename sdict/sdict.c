#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sdict.h"

int string_entry_fn(sdict_entry *entry);
sdict_entry *entry_create(void *key, void *value, unsigned long (*hash_fn)(void *), int (*entry_fn)(sdict_entry *));
int entry_free(sdict_entry **entry, int (*unentry_fn)(sdict_entry *));
void entry_free_recursive(sdict_entry **entry, int (*unentry_fn)(sdict_entry *));
sdict_entry **find_entry(sdict *dict, char *key);
int sdict_remove_nolock(sdict *dict, void *key);


__thread unsigned int error = SDICT_ERR_NONE;


/* djb2 by Dan Bernstein <http://www.cse.yorku.ca/~oz/hash.html> */
unsigned long string_djb2_hashcode(void *key) {
    unsigned long hash = 5381;
    char *str = (char *) key;
    int c;

    while ((c = *str++))
        hash = ((hash << 5) + hash) + c;

    return hash;
}

int string_key_equals(void *s1, void *s2) {
    return strcmp(s1, s2) == 0;
}

int string_entry_fn(sdict_entry *entry) {
    unsigned long len;
    int err;
    size_t size;
    char *value = (char *) entry->value;

    err = string_key_entry_fn(entry);
    if (err) {
        return err;
    }

    len = strlen(value);
    size = len * sizeof(char) + 1;
    entry->value = malloc(size);
    if (!(entry->value)) {
        free(entry->key);
        return SDICT_ERR_MALLOC_FAILURE;
    }
    memcpy(entry->value, value, size);

    return 0;
}

int base_unentry_fn(sdict_entry *entry) {
    free(entry->key);
    free(entry->value);
    return 0;
}

int string_key_entry_fn(sdict_entry *entry) {
    unsigned long len;
    size_t size;
    char *key = (char *) entry->key;

    len = strlen(key);
    size = len * sizeof(char) + 1;
    entry->key = malloc(size);
    if (!(entry->key)) {
        return SDICT_ERR_MALLOC_FAILURE;
    }
    memcpy(entry->key, key, size);

    return 0;
}

int string_key_unentry_fn(sdict_entry *entry) {
    free(entry->key);
    return 0;
}


sdict *sdict_string_create(unsigned int table_length) {
    return sdict_create(string_djb2_hashcode, string_key_equals, string_entry_fn, base_unentry_fn, table_length);
}

sdict *sdict_create(
        unsigned long (*hash_fn)(void *),
        int (*key_equals)(void *, void *),
        int (*entry_fn)(sdict_entry *),
        int (*unentry_fn)(sdict_entry *),
        unsigned int table_length) {

    sdict *dict;
    int err;

    sdict_reset_error();

    dict = malloc(sizeof(sdict));
    if (!dict) {
        error = SDICT_ERR_MALLOC_FAILURE;
        return NULL;
    }

    err = pthread_mutex_init(&(dict->mutex), NULL);
    if (err) {
        free(dict);
        error = SDICT_ERR_MUTEX_FAILURE;
        return NULL;
    }

    dict->hash_fn = hash_fn;
    dict->key_equals = key_equals;
    dict->entry_fn = entry_fn;
    dict->unentry_fn = unentry_fn;
    dict->size = 0;
    dict->table = malloc(table_length * sizeof(sdict_entry *));
    if (!(dict->table)) {
        free(dict);
        error = SDICT_ERR_MALLOC_FAILURE;
        return NULL;
    }
    memset(dict->table, 0, table_length * sizeof(sdict_entry *));
    dict->table_length = table_length;

    return dict;
}


void sdict_free(sdict **dict) {
    sdict_reset_error();
    sdict_free_noreset(dict);
}

void sdict_it(sdict *dict, sdict_iterator *it) {
    sdict_reset_error();

    if (!dict) {
        // dictionary is NULL but the iterator is valid, it just won't cycle
        it->current = NULL;
        return;
    }

    pthread_mutex_lock(&(dict->mutex));
    it->dict = dict;
    for (it->index = 0; it->index < dict->table_length; it->index++) {
        it->current = dict->table[it->index];
        if (it->current) {
            return;
        }
    }

    // dictionary is empty but the iterator is valid, it just won't cycle
    it->current = NULL;
}

void sdict_it_unlock(sdict_iterator *it) {
    if (!it) {
        return;
    }

    it->current = NULL;
    pthread_mutex_unlock(&(it->dict->mutex));
}

void sdict_it_next(sdict_iterator *it) {
    sdict_reset_error();

    if (!it) {
        error = SDICT_ERR_NULL;
        return;
    }

    if (it->current->next) {
        it->current = it->current->next;
        return;
    }

    for (it->index++; it->index < it->dict->table_length; it->index++) {
        it->current = it->dict->table[it->index];
        if (it->current) {
            break;
        }
    }

    if (it->index >= it->dict->table_length) {
        it->index = -1;
        it->current = NULL;
        return;
    }

    it->current = it->dict->table[it->index];
}

void sdict_it_remove(sdict_iterator *it) {
    if (it->current == &(it->dummy)) {
        return;
    }

    it->dummy.next = it->current->next;
    sdict_remove_nolock(it->dict, it->current->key);
    it->current = &(it->dummy);
}


void sdict_free_noreset(sdict **dict) {
    unsigned int i;

    for (i = 0; i < (*dict)->table_length; i++) {
        entry_free_recursive(&((*dict)->table[i]), (*dict)->unentry_fn);
    }

    pthread_mutex_destroy(&((*dict)->mutex));
    free((*dict)->table);
    free(*dict);
    *dict = NULL;
}


void sdict_put(sdict *dict, void *key, void *value) {
    sdict_entry *entry;
    sdict_entry **target;

    sdict_reset_error();

    entry = entry_create(key, value, dict->hash_fn, dict->entry_fn);
    if (!entry) {
        error = SDICT_ERR_MALLOC_FAILURE;
        return;
    }

    pthread_mutex_lock(&(dict->mutex));
    target = &(dict->table[entry->hash % dict->table_length]);
    while (*target && strcmp((*target)->key, entry->key) != 0)
        target = &((*target)->next);

    if (*target) {
        entry->next = (*target)->next;
        entry_free(target, dict->unentry_fn);
    } else {
        dict->size++;
    }

    *target = entry;
    pthread_mutex_unlock(&(dict->mutex));
}

sdict_entry **find_entry(sdict *dict, char *key) {
    unsigned long hash;
    sdict_entry **target;

    hash = dict->hash_fn(key);

    target = &(dict->table[hash % dict->table_length]);
    while ((*target) && ((*target)->hash != hash || !(dict->key_equals((*target)->key, key))))
        target = &((*target)->next);

    return (*target) ? target : NULL;
}


int sdict_remove(sdict *dict, void *key) {
    sdict_reset_error();

    pthread_mutex_lock(&(dict->mutex));
    sdict_remove_nolock(dict, key);
    pthread_mutex_unlock(&(dict->mutex));
    return 1;
}

int sdict_remove_nolock(sdict *dict, void *key) {
    sdict_entry **target;
    sdict_entry *entry;

    target = find_entry(dict, key);
    if (!target)
        return 0;
    entry = *target;

    *target = entry->next;
    entry_free(target, dict->unentry_fn);
    dict->size--;

    return 1;
}


void *sdict_get(sdict *dict, void *key) {
    sdict_entry **target;

    sdict_reset_error();

    pthread_mutex_lock(&(dict->mutex));
    target = find_entry(dict, key);
    pthread_mutex_unlock(&(dict->mutex));
    return target ? (*target)->value : NULL;
}

void sdict_dump(sdict *dict) {
    unsigned int i;
    sdict_entry *target;

    pthread_mutex_lock(&(dict->mutex));
    printf("\n\n::: sdict dump :::\n");
    for (i = 0; i < dict->table_length; i++) {
        if (dict->table[i]) {
            printf("(%d) ::: ", i);

            target = dict->table[i];
            while (target) {
                printf("[ \"%s\" | \"%s\" ] => ", (char *) (target)->key, (char *) (target)->value);
                target = target->next;
            }

            puts("NULL");
        }
    }
    printf("::: sdict dump end :::\n\n");
    pthread_mutex_unlock(&(dict->mutex));
}


sdict_entry *entry_create(void *key, void *value, unsigned long (*hash_fn)(void *), int (*entry_fn)(sdict_entry *)) {
    int fn_err;
    sdict_entry *entry;

    entry = malloc(sizeof(sdict_entry));
    if (!entry) {
        error = SDICT_ERR_MALLOC_FAILURE;
        return NULL;
    }

    entry->key = key;
    entry->value = value;

    if (entry_fn) {
        fn_err = entry_fn(entry);
        if (fn_err) {
            error = SDICT_ERR_ENTRY_FN;
            free(entry);
            return NULL;
        }
    }

    entry->hash = hash_fn(entry->key);
    entry->next = NULL;
    return entry;
}

int entry_free(sdict_entry **entry, int (*unentry_fn)(sdict_entry *)) {
    int fn_err;

    if (!(*entry))
        return 0;

    if (unentry_fn) {
        fn_err = unentry_fn(*entry);
        if (fn_err) {
            return SDICT_ERR_ENTRY_FN;
        }
    }

    free((*entry));
    *entry = NULL;

    return 0;
}

void entry_free_recursive(sdict_entry **entry, int (*unentry_fn)(sdict_entry *)) {
    int fn_err;

    error = 0;

    if (!(*entry))
        return;

    if ((*entry)->next)
        entry_free_recursive(&((*entry)->next), unentry_fn);

    fn_err = entry_free(entry, unentry_fn);
    if (fn_err) {
        error = SDICT_ERR_ENTRY_FN;
    }
}


unsigned int sdict_error(void) {
    return error;
}

void sdict_set_error(unsigned int err) {
    error = err;
}
