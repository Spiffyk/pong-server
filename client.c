#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <libnet.h>

#include "logc/log.h"
#include "util.h"

#include "client.h"

__thread char logid_buffer[256];




int client_dict_entry(sdict_entry *entry) {
    int *key = (int *) entry->key;

    entry->key = malloc(sizeof(int));
    if (!entry->key) {
        return EXIT_FAILURE;
    }
    *((int *) entry->key) = *key;

    return EXIT_SUCCESS;
}

int client_dict_unentry(sdict_entry *entry) {
    free(entry->key);
    client_free((client **) &(entry->value));

    return EXIT_SUCCESS;
}

int client_ping_entry(sdict_entry *entry) {
    entry->key = &(((client_ping *) entry->value)->uuid);
    return 0;
}


int client_ping_unentry(sdict_entry *entry) {
    free(entry->value);
    return 0;
}


client *client_create(int socket, struct sockaddr_in *addr) {
    client *result;

    result = malloc(sizeof(client));
    if (!result) {
        return NULL;
    }

    pthread_mutex_init(&result->mutex, NULL);
    result->client_state = CLIENT_STATE_NONE;
    result->connection_state = CONNECTION_STATE_CONNECTED;
    result->sending_garbage_incidents = 0;
    result->socket = socket;
    result->in_queue = cmq_create();
    result->buffer_wpos = result->buffer;
    result->addr = *addr;
    result->name[0] = '\0';
    result->last_pong = mono_msec();
    result->latency = -1;
    result->pings = sdict_create(uuid_hash, uuid_equals, client_ping_entry, client_ping_unentry, SDICT_DEFAULT_SIZE);

    if (!result->in_queue || !result->pings) {
        pthread_mutex_destroy(&result->mutex);
        cmq_free(&result->in_queue);
        sdict_free(&result->pings);
        free(result);
        return NULL;
    }

    return result;
}


void client_free(client **clt) {
    if (!clt || !(*clt)) {
        return;
    }

    pthread_mutex_destroy(&((*clt)->mutex));
    sdict_free(&((*clt)->pings));
    cmq_free(&((*clt)->in_queue));
    free(*clt);

    *clt = NULL;
}

char *client_logid(client *clt) {
    char address_buffer[64];

    if (clt->name[0]) {
        snprintf(logid_buffer, sizeof(logid_buffer), "%s:%d [%s]",
                inet_ntop(AF_INET, &(clt->addr.sin_addr), address_buffer, sizeof(address_buffer)),
                ntohs(clt->addr.sin_port),
                clt->name);
    } else {
        snprintf(logid_buffer, sizeof(logid_buffer), "%s:%d",
                inet_ntop(AF_INET, &(clt->addr.sin_addr), address_buffer, sizeof(address_buffer)),
                ntohs(clt->addr.sin_port));
    }

    return logid_buffer;
}

char *client_disconnect_reason(client *clt) {
    char *result;

    switch (clt->connection_state) {
        case CONNECTION_STATE_DISCONNECT_LOGOUT:
            result = "Logged out";
            break;
        case CONNECTION_STATE_DISCONNECT_SENDING_GARBAGE:
            result = "Sending garbage";
            break;
        case CONNECTION_STATE_DISCONNECT_COMMUNICATION_ERROR:
            result = "General communication error";
            break;
        case CONNECTION_STATE_DISCONNECT_TIMEOUT:
            result = "Connection timed out";
            break;
        case CONNECTION_STATE_DISCONNECT_INVALID_NAME:
            result = "Invalid login name";
            break;
        case CONNECTION_STATE_DISCONNECT_SERVER_FULL:
            result = "Server full";
            break;
        default:
            result = "Unknown";
    }

    return result;
}
