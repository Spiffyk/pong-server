#ifndef UPS_SP_INMSG_H
#define UPS_SP_INMSG_H

#include <uuid/uuid.h>
#include "game_consts.h"
#include "client_consts.h"

typedef struct {
    unsigned long msec;
    unsigned int type;
    size_t size;
} inmsg;

#define INMSG_TYPE_MASK 0xFF00;

#define INMSG_TYPE_SYS 0x0000;
#define INMSG_TYPE_LOBBY 0x0100;
#define INMSG_TYPE_INGAME 0x0200;

#define INMSG_SYS_LOGIN 0x0001
#define INMSG_SYS_LOGIN_NAME "login"
typedef struct {
    inmsg meta;
    char name[CLIENT_NAME_LENGTH];
} inmsg_sys_login;

#define INMSG_SYS_LOGOUT 0x0002
#define INMSG_SYS_LOGOUT_NAME "logout"

#define INMSG_SYS_PONG 0x0003
#define INMSG_SYS_PONG_NAME "pong"
typedef struct {
    inmsg meta;
    uuid_t uuid;
} inmsg_sys_pong;

#define INMSG_LOBBY_GAME_CREATE 0x0101
#define INMSG_LOBBY_GAME_CREATE_NAME "lobby_game_create"
typedef struct {
    inmsg meta;
    char name[GAME_NAME_LENGTH];
} inmsg_lobby_game_create;

#define INMSG_LOBBY_JOIN 0x0102
#define INMSG_LOBBY_JOIN_NAME "join"
typedef struct {
    inmsg meta;
    uuid_t uuid;
} inmsg_lobby_join;

#define INMSG_LOBBY_REJOIN 0x0103
#define INMSG_LOBBY_REJOIN_NAME "rejoin"
typedef struct {
    inmsg meta;
    uuid_t uuid;
    int key;
} inmsg_lobby_rejoin;

#define INMSG_GAME_LEAVE 0x0201
#define INMSG_GAME_LEAVE_NAME "leave"

#define INMSG_GAME_PADDLE 0x0202
#define INMSG_GAME_PADDLE_NAME "paddle"
typedef struct {
    inmsg meta;
    double ypos;
    int yvel;
} inmsg_game_paddle;

inmsg *parse_message(char *message);

#endif /* UPS_SP_INMSG_H */
