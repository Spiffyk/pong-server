#include <stdlib.h>
#include <string.h>

#include "cmq.h"

#define MSG_BUFFER_SIZE 128

__thread char buffer[MSG_BUFFER_SIZE];

cmq_entry *cmq_entry_create(inmsg *msg);
void cmq_entry_free(cmq_entry **entry);
void cmq_entry_free_recursive(cmq_entry **entry);

cmq *cmq_create(void) {
    cmq *result;
    int err;

    result = malloc(sizeof(cmq));
    if (!result) {
        return NULL;
    }

    err = pthread_mutex_init(&(result->mutex), NULL);
    if (err) {
        free(result);
        return NULL;
    }

    result->head = NULL;
    result->tail = NULL;

    return result;
}

void cmq_free(cmq **queue) {
    if (!queue || !(*queue)) {
        return;
    }

    pthread_mutex_destroy(&((*queue)->mutex));
    cmq_entry_free_recursive(&((*queue)->head));
    free(*queue);
    *queue = NULL;
}

cmq_entry *cmq_entry_create(inmsg *msg) {
    cmq_entry *result;

    result = malloc(sizeof(cmq_entry));
    if (!result) {
        return NULL;
    }

    result->next = NULL;
    result->msg = malloc(msg->size);
    if (!result->msg) {
        free(result);
        return NULL;
    }

    memcpy(result->msg, msg, msg->size);
    return result;
}

void cmq_entry_free(cmq_entry **entry) {
    if (!entry || !(*entry)) {
        return;
    }

    free((*entry)->msg);
    free(*entry);
    *entry = NULL;
}

void cmq_entry_free_recursive(cmq_entry **entry) {
    if (!entry || !(*entry)) {
        return;
    }

    if ((*entry)->next) {
        cmq_entry_free_recursive(&((*entry)->next));
    }

    cmq_entry_free(entry);
}

inmsg *cmq_peek(cmq *queue) {
    if (!queue->head) {
        return NULL;
    }

    pthread_mutex_lock(&queue->mutex);
    memcpy(buffer, queue->head->msg, queue->head->msg->size);
    pthread_mutex_unlock(&queue->mutex);
    return (inmsg *) buffer;
}

inmsg *cmq_pop(cmq *queue) {
    cmq_entry *old_head;
    inmsg *result;

    result = cmq_peek(queue);
    if (!result) {
        return NULL;
    }

    pthread_mutex_lock(&queue->mutex);
    old_head = queue->head;
    queue->head = old_head->next;
    if (queue->tail == old_head) {
        queue->tail = NULL;
    }
    pthread_mutex_unlock(&queue->mutex);

    cmq_entry_free(&old_head);
    return result;
}

int cmq_push(cmq *queue, inmsg *msg) {
    cmq_entry *entry = cmq_entry_create(msg);
    if (!entry) {
        return EXIT_FAILURE;
    }

    pthread_mutex_lock(&queue->mutex);

    if (!queue->head) {
        queue->head = entry;
        queue->tail = entry;
    } else {
        queue->tail->next = entry;
        queue->tail = entry;
    }

    pthread_mutex_unlock(&queue->mutex);
    return EXIT_SUCCESS;
}
