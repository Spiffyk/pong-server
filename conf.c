#include <stdlib.h>
#include <string.h>
#include "logc/log.h"
#include "conf.h"

int read_log_level(char *strlevel);

int read_log_level(char *const strlevel) {
    if (!strlevel) {
        return LOG_INFO;
    }

    if (strcmp("TRACE", strlevel) == 0) return LOG_TRACE;
    if (strcmp("DEBUG", strlevel) == 0) return LOG_DEBUG;
    if (strcmp("INFO", strlevel) == 0) return LOG_INFO;
    if (strcmp("WARN", strlevel) == 0) return LOG_WARN;
    if (strcmp("ERROR", strlevel) == 0) return LOG_ERROR;
    if (strcmp("FATAL", strlevel) == 0) return LOG_FATAL;

    return LOG_INFO;
}

conf *conf_create(void) {
    conf *result;

    result = malloc(sizeof(conf));
    if (!result) {
        return NULL;
    }

    result->log_level = LOG_INFO;
    strcpy(result->log_file, "server.log");

    result->server_motd[0] = '\0';
    result->server_port = 2333;
    result->server_address[0] = '\0';
    result->server_max_clients = 64;

    result->game_field_width = 100;
    result->game_field_height = 60;
    result->game_paddle_margin = 5;
    result->game_paddle_width = 3;
    result->game_paddle_height = 11;
    result->game_paddle_speed = 10;
    result->game_ball_size = 3;
    result->game_ball_speed = 8;
    result->game_win_score = 10;

    return result;
}

conf *conf_from_sdict(sdict *dict) {
    conf *result;
    void *dvalue;

    result = conf_create();
    if (!result) {
        return NULL;
    }

    dvalue = sdict_get(dict, "log.level");
    if (dvalue) result->log_level = read_log_level(dvalue);

    dvalue = sdict_get(dict, "log.file");
    if (dvalue) strncpy(result->log_file, dvalue, sizeof(result->log_file));

    dvalue = sdict_get(dict, "server.motd");
    if (dvalue) strncpy(result->server_motd, dvalue, sizeof(result->server_motd));

    dvalue = sdict_get(dict, "server.port");
    if (dvalue) result->server_port = atoi(dvalue);

    dvalue = sdict_get(dict, "server.address");
    if (dvalue) strncpy(result->server_address, dvalue, sizeof(result->server_address));

    dvalue = sdict_get(dict, "server.max_clients");
    if (dvalue) result->server_max_clients = atoi(dvalue);

    dvalue = sdict_get(dict, "game.field.width");
    if (dvalue) result->game_field_width = atof(dvalue);

    dvalue = sdict_get(dict, "game.field.height");
    if (dvalue) result->game_field_height = atof(dvalue);

    dvalue = sdict_get(dict, "game.paddle.margin");
    if (dvalue) result->game_paddle_margin = atof(dvalue);

    dvalue = sdict_get(dict, "game.paddle.width");
    if (dvalue) result->game_paddle_width = atof(dvalue);

    dvalue = sdict_get(dict, "game.paddle.height");
    if (dvalue) result->game_paddle_height = atof(dvalue);

    dvalue = sdict_get(dict, "game.paddle.speed");
    if (dvalue) result->game_paddle_speed = atof(dvalue);

    dvalue = sdict_get(dict, "game.ball.size");
    if (dvalue) result->game_ball_size = atof(dvalue);

    dvalue = sdict_get(dict, "game.ball.speed");
    if (dvalue) result->game_ball_speed = atof(dvalue);

    dvalue = sdict_get(dict, "game.win_score");
    if (dvalue) result->game_win_score = atoi(dvalue);

    return result;
}

void conf_free(conf **c) {
    if (!c || !*c) {
        return;
    }

    free(*c);
    *c = NULL;
}
